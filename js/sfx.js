var html5_audiotypes={
	"mp3": "audio/mpeg",
	"mp4": "audio/mp4",
	"ogg": "audio/ogg",
	"wav": "audio/wav"
}

function createsoundbite(sound){
	var html5audio=document.createElement('audio')
	if (html5audio.canPlayType){ //check support for HTML5 audio
		for (var i=0; i<arguments.length; i++){
			var sourceel=document.createElement('source')
			sourceel.setAttribute('src', arguments[i])
			if (arguments[i].match(/\.(\w+)$/i))
				sourceel.setAttribute('type', html5_audiotypes[RegExp.$1])
			html5audio.appendChild(sourceel)
		}
		html5audio.load()
		html5audio.playclip=function(){
			html5audio.pause()
			html5audio.currentTime=0
			html5audio.play()
		}
		return html5audio
	}
	else{
		return {playclip:function(){throw new Error("Your browser doesn't support HTML5 audio unfortunately")}}
	}
}

//Initialize two sound clips with 1 fallback file each:

//var mouseoversound=createsoundbite("whistle.ogg", "whistle.mp3")
var clicksound = createsoundbite("sfx/button.ogg", "sfx/button.mp3")
var bijisound = createsoundbite("sfx/biji2.ogg", "sfx/biji2.mp3");
var biji2sound = createsoundbite("sfx/biji3.ogg", "sfx/biji3.mp3");
var warn = createsoundbite("sfx/warning.ogg","sfx/warning.mp3");
var lose = createsoundbite("sfx/lose.ogg","sfx/lose.mp3");
var bgm = createsoundbite("bgm/music.mp3");
var win = createsoundbite("sfx/win.ogg","sfx/win.mp3");

function muteMusik()
{
	clicksound.playclip();
        if ($("#mute-music .ui-btn-text").text() === "Music ON") {
            bgm.pause();
            $("#mute-music .ui-btn-text").text('Music OFF');
        } else {
            bgm.playclip();
            $("#mute-music .ui-btn-text").text('Music ON');
        }
}